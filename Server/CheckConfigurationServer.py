import json
from sensornetwork.Configuration.Configuration import Configuration


class CheckConfigurationServer():

    def handleData(self, inputData):
    	configurationData = Configuration.loadConfiguration('data.json')
    	inputData = json.loads(inputData)
    	for device in configurationData:
    		if device["deviceId"] == inputData["deviceId"]:
    			if device["changeConfiguration"] == "True":
    				device["changeConfiguration"] = "False"
    				Configuration.saveConfiguration(configurationData, 'data')
    				device.pop('changeConfiguration')
    				return json.dumps(device)

    			else:
    				return json.dumps({
    				"change": "False"
    				})
        
        return json.dumps({
            "change": "Wrong device id"
            })

            
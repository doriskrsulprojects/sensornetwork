from sensornetwork.Configuration.Configuration import Configuration
import json 

class NotificationServer():

    def handleData(self):
        notifications = Configuration.loadConfiguration('androidNotifications.json')

        if notifications["condition"] == "True":
        	response = json.dumps(notifications)
	        notifications = {
	            "condition": "False", 
	            "notifications": []
	            }
        	Configuration.saveConfiguration(notifications, 'androidNotifications')
        
        else:
        	response = json.dumps({"condition": "False"})

        return response
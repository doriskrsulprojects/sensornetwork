# file: inquiry.py
# auth: Albert Huang <albert@csail.mit.edu>
# desc: performs a simple device inquiry followed by a remote name request of
#       each discovered device
# $Id: inquiry.py 401 2006-05-05 19:07:48Z albert $
#

#import bluetooth #odkomentirat na rpi
import json

class DetectBluetoothDevices():
    def getDevices(self):
        #print("performing inquiry...")

        nearby_devices = bluetooth.discover_devices(
                duration=8, lookup_names=True, flush_cache=True)

        #print("found %d devices" % len(nearby_devices))
        
        """ za imena i mac adrese
        if(len(nearby_devices)!=0):
            response = "{"
            for addr, name in nearby_devices:
                try:
                   # print("  %s - %s" % (addr, name))
                    response+= '\"' + name + '\":\"' + addr + '\",'

                except UnicodeEncodeError:
                   # print("  %s - %s" % (addr, name.encode('utf-8', 'replace')))
                    response+= '\"' + name + '\":\"' + addr + '\",'

            response = response[:-1]
            response+= "}"
        else:
            response="none"
            """

     
	return len(nearby_devices)    

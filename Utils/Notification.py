import shlex
from subprocess import Popen, PIPE
from sensornetwork.Sensors.SensorBase import SensorBase

class Notification():
    @classmethod
    def checkCondition(cls, sensorData, condition):

        for observation in sensorData["data"]:
            observationName = observation

        if(sensorData["sensorType"]!= condition["sensorType"]):
            #print "nije isti sensorType \n"
            return
        elif(sensorData["sensorId"]!= condition["sensorId"]):
            #print "nije isti sensorID\n"
            return
        elif(sensorData["deviceId"]!= condition["deviceId"]):
            #print "nije isti deviceId"
            return
        else:
            return SensorBase.validateCondition(sensorData, condition, observationName)
            
    
    @classmethod
    def sendNotification(cls, data): #izbrisana je sendMail.pl skripta
        #subProcess = Popen(["perl", "sensornetwork/Utils/sendMail.pl", data], stdout=PIPE, stderr=PIPE, shell=False)
        #responseMessage, responseErrorMessage = subProcess.communicate()
        #exitCode = subProcess.returncode
        #return exitCode, responseMessage, responseErrorMessage
        return


        
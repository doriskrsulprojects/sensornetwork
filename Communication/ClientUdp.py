#! python
from sensornetwork.Communication.Client import Client
from sensornetwork.Configuration.Configuration import Configuration
import socket
import sys
import json
import errno

class ClientUdp(Client): 
    #konstruktor prima host i port na kojeg ce slati podatke i otvara socket preko kojeg ce se odvijati komunikacija
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    #slanje podataka na server koji slusa na zadanom hostu i portu
    def sendData(self, data, configuration):
        #self.sock.sendto((json.dumps(data)), (self.host, self.port)) ##samo ova linija ako se testira clientTest.py
        
        if (configuration == 0):
            self.sock.sendto((json.dumps(data)), (self.host, self.port))
        else:
            self.sock.sendto(json.dumps(data), (self.host, self.port))
            
        #slanje se odvija svakih 5ms
        self.sock.settimeout(5)
        try: 
            received = self.sock.recv(1024)
            #ispis odgovora sa servera, npr {"success": "true"}
        
            if(configuration == 0): #ako su se slali podaci sa senzora
                print received
            else: #ako se provjeravala konfiguracija
                if 'change' not in json.loads(received): #ako postoje promjene
                    Configuration.saveConfiguration(json.loads(received), 'dataRPI')
                    print "Configuration saved"
                else:
                    print "No configuration changes"
        except socket.error: #u slucaju socket timeouta
            print "Success: false"
            pass

    

from sensornetwork.Configuration.Configuration import Configuration
from datetime import datetime
import time
import requests
import json

class SensorUp():

	@classmethod
	def formatTime(cls, currentTime):
		timestamp = currentTime
		formattedTimestamp = datetime.fromtimestamp(timestamp)
		formattedTimestampString = str(formattedTimestamp)
		#print "Debug time: " + formattedTimestampString;
		formattedTime = datetime.strptime(formattedTimestampString, '%Y-%m-%d %H:%M:%S.%f').strftime('%Y-%m-%dT%H:%M:%SZ')
		return formattedTime

	@classmethod
	def handleData(cls, sensorData, sensorResult):

		datastreams = Configuration.loadConfiguration('datastreams.json')

		for datastream in datastreams:
			if datastream['datastreamId'] == sensorData['sensorParameters']['datastreamId']:
				datastreamIotId = datastream['iot.id']
				break

		timestampFromServer = SensorUp.formatTime((datetime.utcnow() - datetime(1970, 1, 1)).total_seconds())
		timestampFromSensor = SensorUp.formatTime(sensorData['timestamp'])

		return {
              "phenomenonTime": timestampFromServer,
              "resultTime" : timestampFromSensor,
              "result" : sensorResult,
              "Datastream":{
                "@iot.id": datastreamIotId
                }
        }
        

	@classmethod   
	def getData(cls, resource="", id=""): 
		getUrl = "https://academic-hr-ad60.sensorup.com/display/v1.0"

		if resource:
			getUrl += resource
		if id:
			getUrl += "(" + id + ")"

		response = requests.get(getUrl)
		return response

	@classmethod
	def sendData(cls, sentData, resource="", id=""):
		postUrl = "https://academic:8b59150a@academic-hr-ad60.sensorup.com/v1.0"
		headers = {"Content-Type": "application/json"}

		if resource and id:
			postUrl += resource + "(" + id + ")"
		elif resource:
			postUrl += "/" + resource

		response = requests.post(postUrl, data=json.dumps(sentData), headers=headers)
		return response
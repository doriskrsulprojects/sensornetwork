#! python
from sensornetwork.Communication.ClientUdp import ClientUdp
import json

class DataSender():
	#varijabla u koju spremamo instancu klase za slanje, moze biti bilo koja instanca
    clientSender = None

    def sendOverUdp(self, host, port):
    	self.host = host
    	self.port = port
    	self.clientSender = ClientUdp(self.host, self.port)

    def sendData(self, sensorId, data, configuration):
	    #zapakiraj podatke
	    if(configuration == 0):
	    	sensorData = self.clientSender.sensorDataPacking(sensorId, data)
	    else:
	    	sensorData = self.clientSender.checkConfigurationPacking(data)
	  
	    #posalji zapakirane podatke ako je instaca klintske klase napravljena
	    if self.clientSender:
	    	self.clientSender.sendData(sensorData, configuration)
	    else:
			print "Client connection failed"
